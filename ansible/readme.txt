# Installation docker to VM

Run installing docker, change directory -> ansible:

```bash    
    ansible-playbook docker.yaml -i configs/hosts.yaml
```
# Run docker container in VM

Run container use this:
    
```bash
    ansible-playbook run.yaml -i configs/hosts.yaml
```

Before you need change "configs/hosts.yaml", add new public ip address

# Connection

That create ssh connect to VM, you need extaract the key from the Terraform

In directory terraform-azure, run this command:

```bash
    terraform output -raw tls_private_key > id_rsa
```