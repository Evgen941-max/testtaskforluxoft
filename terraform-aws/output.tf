output "associate-public-ip" {
    value = aws_instance.demoinstance[*].associate_public_ip_address
}

output "public-ip" {
  value = aws_instance.demoinstance[*].public_ip
}